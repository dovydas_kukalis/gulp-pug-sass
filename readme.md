# Structure
Source files are located in `src`. All compiled files are in `dist`.

# Installing
`npm install`

# Running
`gulp`

Running this command gulp will automatically watch over file changes inside `src`, recompile changed files to `dist` and refresh browser.

# Deploying
`gulp deploy-dev` deploys css to s3 bucket `/dev/main.css`

`gulp deploy-prod` deploys to s3 bucket `/app/main.css`

# Other stuff
It automatically adds all vendor prefixes in compiled CSS so you don't need to do it in SCSS. It also minifies CSS, JS and images. So images are best to be copied from `dist` to website because savings are usually huge, around 70%.
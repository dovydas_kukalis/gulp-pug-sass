var gulp = require('gulp'),
  sass = require('gulp-sass'),
  browserSync = require('browser-sync'),
  autoprefixer = require('gulp-autoprefixer'),
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename'),
  cssnano = require('gulp-cssnano'),
  pug = require('gulp-pug'),
  package = require('./package.json'),
  index = require('gulp-index'),
  imagemin = require('gulp-imagemin'),
  fs = require('fs'),
  cache = require('gulp-cached');

var s3Config = JSON.parse(fs.readFileSync('private/s3.json'));
var s3 = require('gulp-s3-upload')(s3Config);


gulp.task('css', function () {
  return gulp.src('src/assets/css/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer('last 4 version'))
    .pipe(gulp.dest('dist/assets/css'))
    .pipe(cssnano())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('dist/assets/css'))
});

gulp.task('js', function () {
  return gulp.src('src/assets/js/app.js')
    .pipe(gulp.dest('dist/assets/js'))
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('dist/assets/js'))
});

gulp.task('pug', function () {
  return gulp.src('src/templates/pages/**/*.pug', { base: './src/templates/pages' })
    .pipe(cache('pug'))
    .pipe(pug({ pretty: true }))
    .pipe(gulp.dest('dist/'))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('img', function () {
  return gulp.src('src/assets/img/**/*')
    .pipe(cache('img'))
    .pipe(imagemin())
    .pipe(gulp.dest('dist/assets/img/'))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('browser-sync', function () {
  browserSync.init(null, {
    server: {
      baseDir: "dist"
    }
  });
});

gulp.task('deploy-dev', ['css'], function() {
  gulp.src("./dist/assets/css/main.min.css")
    .pipe(rename('main.css'))
    .pipe(s3({
      Bucket: 'plutio/dev',
      ACL:    'public-read',
      CacheControl: "max-age=0"
    }, {
      maxRetries: 5
    }))
  ;
});

gulp.task('deploy-prod', ['css'], function() {
  gulp.src("./dist/assets/css/main.min.css")
    .pipe(rename('main.css'))
    .pipe(s3({
      Bucket: 'plutio/app',
      ACL:    'public-read',
      CacheControl: "max-age=0"
    }, {
      maxRetries: 5
    }))
  ;
});

gulp.task('buildIndex', ['pug'], function () {
  return gulp.src('./dist/**/*.html')
    .pipe(index({
      relativePath: './dist',
      'prepend-to-output': function () {
        return '<head><link rel="stylesheet" type="text/css" href="/assets/css/main.css"></head><body>';
      },
      'section-template': function (sectionContent) {
        return '<div class="content"><div class="section"><div class="section-body"><div class="list-section"><div class="group-container">' + sectionContent + '</div></div></div></div></div>';
      },
      'section-heading-template': function (heading) {
        return '<div class="group-head"><p>' + heading + '</p></div>';
      },
      'list-template': function (listContent) {
        return '<div class="group-content">' + listContent + '</div>';
      },
      'item-template': function (filepath, filename) {
        return '<div class="list-item"><a class="list-item-cell link" href="' + (filepath || "") + '/' + filename + '"><span class="list-title">' + filename + '</span></a></div>';
      }
    }))
    .pipe(gulp.dest('./dist'));
});

gulp.task('bs-reload', function () {
  browserSync.reload();
});

gulp.task('default', ['css', 'js', 'pug', 'img', 'browser-sync', 'buildIndex'], function () {
  gulp.watch("src/assets/css/*", ['css']);
  gulp.watch("src/assets/js/*.js", ['js']);
  gulp.watch("src/templates/**/*.pug", ['pug']);
  gulp.watch("src/assets/img/**/*", ['img']);
  gulp.watch("dist/*.html", ['bs-reload']);
  gulp.watch("dist/assets/css/main.css", ['bs-reload']);
  gulp.watch("dist/assets/js/app.js", ['bs-reload']);
});
